const express = require("express");
const app = express();

const morgan = require("morgan");
const { engine: handlebars } = require("express-handlebars");
const path = require("path");
const route = require("./routes/index.route");

const port = 3000;

app.use(express.static(path.join(__dirname, "public")));
app.use(
    express.urlencoded({
        extended: true,
    })
);
app.use(express.json());

// http logger
// app.use(
//     morgan("combined", {
//         // skip: function (req, res) {
//         //     return res.statusCode < 400;
//         // },
//     })
// );

// template engine
app.engine(".hbs", handlebars({ extname: ".hbs" }));
app.set("view engine", ".hbs");
app.set("views", path.join(__dirname, "resources/views"));

// routes
route(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
